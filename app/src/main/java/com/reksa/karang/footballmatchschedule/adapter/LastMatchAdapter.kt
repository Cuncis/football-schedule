package com.reksa.karang.footballmatchschedule.adapter

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.reksa.karang.footballmatchschedule.R
import com.reksa.karang.footballmatchschedule.model.LastMatch
import kotlinx.android.synthetic.main.row_last_match.view.*

class LastMatchAdapter(private val listEvents: List<LastMatch>, private val listener: (LastMatch) -> Unit): RecyclerView.Adapter<LastMatchAdapter.LastMatchViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LastMatchViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.row_last_match, parent, false)

        return LastMatchViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listEvents.size
    }

    override fun onBindViewHolder(holder: LastMatchViewHolder, position: Int) {

        holder.bindItem(listEvents[position])

    }

    inner class LastMatchViewHolder(val view: View): RecyclerView.ViewHolder(view) {

        private val dateEvent = view.tv_date
        private val homeTeam = view.tv_homeTeam
        private val homeScore = view.tv_homeScore
        private val awayTeam = view.tv_awayTeam
        private val awayScore = view.tv_awayScore

        fun bindItem(event: LastMatch) {
            dateEvent.text = event.dateEvent
            homeTeam.text = event.homeTeam
            homeScore.text = event.homeScore
            awayTeam.text = event.awayTeam
            awayScore.text = event.awayScore
            itemView.setOnClickListener {
                Log.d("LastMatchHolder", "Clicked")
            }
        }

    }

}