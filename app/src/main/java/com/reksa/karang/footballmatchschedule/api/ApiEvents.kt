package com.reksa.karang.footballmatchschedule.api

import com.reksa.karang.footballmatchschedule.BuildConfig
import com.reksa.karang.footballmatchschedule.model.LastMatch
import com.reksa.karang.footballmatchschedule.model.LastMatchResponse
import retrofit2.http.GET
import rx.Observable

interface ApiEvents {
    @GET("api/v1/json/1/eventspastleague.php?id=4328")
        fun getEvents(): Observable<LastMatchResponse>
    }