package com.reksa.karang.footballmatchschedule.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import com.google.gson.Gson

import com.reksa.karang.footballmatchschedule.R
import com.reksa.karang.footballmatchschedule.adapter.LastMatchAdapter
import com.reksa.karang.footballmatchschedule.api.ApiEvents
import com.reksa.karang.footballmatchschedule.model.LastMatch
import kotlinx.android.synthetic.main.fragment_last_match.*
import kotlinx.android.synthetic.main.fragment_last_match.view.*
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 *
 */
class LastMatchFragment : Fragment(){

    private lateinit var adapter: LastMatchAdapter
    private val listEvents: MutableList<LastMatch> = mutableListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val roowView = inflater.inflate(R.layout.fragment_last_match, container, false)

        return roowView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recyclerview = view.rv_last_match

        adapter = LastMatchAdapter(listEvents){
            Toast.makeText(activity, "", Toast.LENGTH_LONG).show()
        }
        recyclerview.adapter = adapter
        recyclerview.layoutManager = LinearLayoutManager(context)

        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://www.thesportsdb.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .build()

        val apiEvents = retrofit.create(ApiEvents::class.java)

        apiEvents.getEvents()
            .subscribeOn(Schedulers.newThread())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                this.setEvents(it.match)
            }, {
                Log.d("Last match", "error: " + it.message)
            })
    }

    private fun setEvents(data: List<LastMatch>) {
        listEvents.clear()
        listEvents.addAll(data)
        adapter.notifyDataSetChanged()
    }
}
