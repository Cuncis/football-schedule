package com.reksa.karang.footballmatchschedule.model

import com.google.gson.annotations.SerializedName

data class LastMatchResponse(@SerializedName("events")
                             val match: List<LastMatch>)